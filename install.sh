#! /bin/bash
echo -e "\n#################### IPTGui INSTALLER ####################"
echo "------ Università degli Studi di Napoli Federico II ------"
echo "-------------- Umberto Ferrandino - M63/503 --------------"
echo "------------- u.ferrandino@studenti.unina.it -------------"
echo -e "##########################################################\n"

echo "Please be careful using IPTGui. It's a totally prototypal software!"
echo -e "Use at your own risk.\n"
echo -e "Please, before proceeding with the installation, read the README.md file!.\n"

#echo -n "Enter some text > "
#read text
#echo "You entered: $text"
IFS=': ' read -r -a paths <<< "$PATH"

#Check for Java

echo -n "Check for Java installation..."

javacPath=$(type -p javac)
javaPath=$(type -p java)

if [ -z $javaPath ] || [ -z $javacPath ]; then
    echo "Failed"
    echo "Please install Java!"
    exit 1
else
    echo "OK"
fi
    
#Check for kdesudo or gksudo

echo -n "Check for kdesudo or gksudo..."

kdesudoCheck=0
gksudoCheck=0
gksuCheck=0

for element in "${paths[@]}"
do
  FILE=$element/kdesudo
  FILE_2=$element/gksudo
  FILE_3=$element/gksu
  if [ -f $FILE ]; then
    kdesudoCheck=1
    sudo=kdesudo
    break
  elif [ -f $FILE_2 ]; then
    gksudoCheck=1
    sudo=gksudo
    break
  elif [ -f $FILE_3 ]; then
    gksuCheck=1
    sudo=gksu
    break
  fi   
done

if [ $(($kdesudoCheck + $gksudoCheck + $gksuCheck)) == 1 ]; then
  echo "OK"
else
  sudoCmd=$(type -p sudo)
  if [ -z $sudo ]; then
      echo "Failed"
      echo "Please install 'kdesudo' (for KDE users), 'gksuo' (for GNOME) or at least 'sudo'!"
      exit 1
  else
      echo "Warning"
      echo "------>No kdesudo or gksudo founded...Using sudo. IPTGui may not work."
  fi
fi

#Check for iptables

echo -n "Check for iptables..."

rootPath=/sbin:/bin:/usr/sbin:/usr/bin
IFS=': ' read -r -a paths <<< "$rootPath"

iptablesCheck=0

for element in "${paths[@]}"
do
  FILE=$element/iptables
  if [ -f $FILE ]; then
    iptablesCheck=1
    break
  fi    
done

if [ $iptablesCheck == 1 ]; then
  echo "OK"
else
  echo "Failed"
  echo "Please install iptables!"
  exit 1
fi

#Insert installation path

echo -n "Please, enter absolute installation path: a folder named IPTGui will be created. Press 'enter' to use default. [current folder is default] > "
read installPath

#Installation

if [ -z $installPath ]; then #Default folder

  installPath="$PWD"/
  echo -e "sudo:"$sudo"\nlogPath:"$installPath"log\npath:"$installPath > "$installPath".config
  
else	#User custom path

  #Check slash at the end of the path

  case "$installPath" in
  */)
      installPath="$installPath"IPTGui/
      ;;
  *)
      installPath=$installPath/IPTGui/
      ;;
  esac 
  mkdir $installPath
  cp ./IPTGui.jar $installPath
  cp ./iptgui_small.png $installPath
  echo -e "sudo:"$sudo"\nlogPath:"$installPath"log\npath:"$installPath > $installPath.config
  
fi

echo "               _______________________"
echo "             < Installation Completed! > "
echo "               ----------------------- "
echo "         (__)   /"
echo "         (oo)  / "
echo "  /-------\/  "
echo " / |     ||    "
echo "*  ||w---||    "

echo -en "Do you want to run the program ? (Y/N) > "
read resp
if [ "$resp" == 'Y' ] || [ "$resp" == 'y' ] || [ "$resp" == "Yes" ] || [ "$resp" == "YES" ] || [ "$resp" == "yes" ]; then
  cd $installPath
  java -jar "$installPath"IPTGui.jar &
fi
echo -e "#! /bin/bash\n$sudo 'java -jar "$installPath"IPTGui.jar &'" > "$installPath"run.sh
chmod +x "$installPath"run.sh
echo "Please use the 'run.sh' script to run IPTGUi! Or simply run this command: "$sudo" 'java -jar "$installPath"IPTGui.jar &'"
