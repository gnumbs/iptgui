package gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;

import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.SpringLayout;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;

import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import java.awt.event.ItemEvent;

public class IPTGuiMain {

	private JFrame frmIptablesGui;
	public static String sudo;
	private static JTable table;
	private static String installationPath;
	private static List<Map<String, String>> data = new ArrayList<Map<String,String>>();
	private static List<Map<String, String>> dataNat = new ArrayList<Map<String,String>>();
	public static LogUtility log;
	public static final boolean _DEBUG_ = false;
	private static boolean startup=true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IPTGuiMain window = new IPTGuiMain();
					window.frmIptablesGui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public IPTGuiMain() {
		try {
			File config = new File("./.config");
			if (!config.exists()) {
				JOptionPane.showMessageDialog(null, "No configuration file founded!", "Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			BufferedReader br = new BufferedReader(new FileReader(config));
			sudo = br.readLine().split(":")[1]+" ";
			log = new LogUtility(br.readLine().split(":")[1], "iptables_log.log");
			installationPath = br.readLine().split(":")[1];
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		initialize();
		loadFilter(); //Carica le regole nella tabella firewalling
	}
	
	public static void loadFilter() {
		String s;
		String temp = "";
		String [] output = null;
		Process p;
		int count=0;
		boolean reload=true;

		data.clear();
		
		/**************************************************
		 * IPTGui.sh
		 * Load the Filter INPUT chain
		 **************************************************/
		
        try {
        	File file = new File("./IPTGui.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -nvL INPUT --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
//        	if (!file.isFile()) {
        	PrintWriter writer = new PrintWriter("./IPTGui.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
//        	}
        	
        	/* Controllo che la MD5 sum sia quella che mi aspetto,
        	 * per assicurarmi che lo script non sia stato 
        	 * modificato. Gli script vengono eseguiti come root,
        	 * pertanto modificarli significherebbe possibilità di
        	 * root escalation. 
        	 */
        	
//    		MessageDigest md = MessageDigest.getInstance("MD5");
//    		byte[] cmdDigest = md.digest(("iptGUI_"+cmd[1]).getBytes("UTF-8"));
//    		byte[] fileDigest = new byte[(int) file.length()];
//    		FileInputStream fs= new FileInputStream("./filterInput_load.sh");
//    		BufferedReader brDigest = new BufferedReader(new InputStreamReader(fs));
//    		brDigest.readLine(); //Lose the first line
//    		fileDigest = md.digest(("iptGUI_"+brDigest.readLine()).getBytes("UTF-8"));
    		
//    		if (Arrays.equals(fileDigest, cmdDigest) && (brDigest.readLine()==null)) {
//    			
        	if (startup) {
        		p = Runtime.getRuntime().exec(sudo+"-i "+installationPath+"iptgui_small.png"+" ./IPTGui.sh");
        		startup=false;
        	}
        	else
        		p = Runtime.getRuntime().exec(sudo+"./IPTGui.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));

        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "INPUT");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		data.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "INPUT", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        	//    		}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		/**************************************************
		 * filterOutput_load.sh
		 * Load the Filter OUTPUT chain
		 **************************************************/
        
        try {
        	File file = new File("./filterOutput_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -nvL OUTPUT --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
        	PrintWriter writer = new PrintWriter("./filterOutput_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
		
        	p = Runtime.getRuntime().exec(sudo+"./filterOutput_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));
        	//    			data.clear();
        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "OUTPUT");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		data.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "OUTPUT", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		/**************************************************
		 * filterForward_load.sh
		 * Load the Filter FORWARD chain
		 **************************************************/
        
        try {
        	File file = new File("./filterForward_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -nvL FORWARD --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
        	PrintWriter writer = new PrintWriter("./filterForward_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
        	    			
        	p = Runtime.getRuntime().exec(sudo+"./filterForward_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));
        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "FORWARD");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		data.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "FORWARD", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        	if (count==0) { //Nel caso non avessimo elementi in tabella...
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		model.setRowCount(0);
        	} else {
        		count=0;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
	}
	public static void loadNat() {
		String s;
		String temp = "";
		String [] output = null;
		Process p;
		int count=0;
		boolean reload=true;

		dataNat.clear();
		DefaultTableModel m = (DefaultTableModel) table.getModel();
		m.setRowCount(0);
		
		/**************************************************
		 * natPre_load.sh
		 * Load the Nat PREROUTING chain
		 **************************************************/
		
        try {
        	File file = new File("./natPre_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -t nat -nvL PREROUTING --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
//        	if (!file.isFile()) {
        	PrintWriter writer = new PrintWriter("./natPre_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
//        	}
        	
        	/* Controllo che la MD5 sum sia quella che mi aspetto,
        	 * per assicurarmi che lo script non sia stato 
        	 * modificato. Gli script vengono eseguiti come root,
        	 * pertanto modificarli significherebbe possibilità di
        	 * root escalation. 
        	 */
        	
//    		MessageDigest md = MessageDigest.getInstance("MD5");
//    		byte[] cmdDigest = md.digest(("iptGUI_"+cmd[1]).getBytes("UTF-8"));
//    		byte[] fileDigest = new byte[(int) file.length()];
//    		FileInputStream fs= new FileInputStream("./filterInput_load.sh");
//    		BufferedReader brDigest = new BufferedReader(new InputStreamReader(fs));
//    		brDigest.readLine(); //Lose the first line
//    		fileDigest = md.digest(("iptGUI_"+brDigest.readLine()).getBytes("UTF-8"));
    		
//    		if (Arrays.equals(fileDigest, cmdDigest) && (brDigest.readLine()==null)) {
//    			
        	p = Runtime.getRuntime().exec(sudo+"./natPre_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));

        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "PREROUTING");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		dataNat.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "PREROUTING", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        	//    		}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		/**************************************************
		 * natIn_load.sh
		 * Load the Nat INPUT chain
		 **************************************************/
        
        try {
        	File file = new File("./natIn_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -t nat -nvL INPUT --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
        	PrintWriter writer = new PrintWriter("./natIn_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
		
        	p = Runtime.getRuntime().exec(sudo+"./natIn_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));
        	//    			data.clear();
        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "INPUT");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		dataNat.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "INPUT", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		/**************************************************
		 * natOut_load.sh
		 * Load the Nat OUTPUT chain
		 **************************************************/
        
        try {
        	File file = new File("./natOut_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -t nat -nvL OUTPUT --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
        	PrintWriter writer = new PrintWriter("./natOut_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
        	    			
        	p = Runtime.getRuntime().exec(sudo+"./natOut_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));
        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "OUTPUT");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		dataNat.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "OUTPUT", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		/**************************************************
		 * natPost_load.sh
		 * Load the Nat OUTPUT chain
		 **************************************************/
        
        try {
        	File file = new File("./natPost_load.sh");
        	String[] cmd = {"#! /bin/sh", "iptables -t nat -nvL POSTROUTING --line-numbers | tail -n +3 | sed -e 's/ * / /g'"};
        	
        	PrintWriter writer = new PrintWriter("./natPost_load.sh", "UTF-8");
        	writer.println(cmd[0]+"\n"+cmd[1]);
        	writer.close();
        	file.setWritable(true);
        	file.setExecutable(true);
        	    			
        	p = Runtime.getRuntime().exec(sudo+"./natPost_load.sh");
        	BufferedReader br = new BufferedReader(
        			new InputStreamReader(p.getInputStream()));
        	while ((s = br.readLine()) != null) {
        		Map<String, String> firewallMap = new HashMap<String, String>();
        		output = s.split(" ");
        		count++;
        		for (int j=10; j<output.length; j++)
        			temp += output[j]+" ";
        		firewallMap.put("index", output[0]);
        		firewallMap.put("chain", "POSTROUTING");
        		firewallMap.put("in", output[6]);
        		firewallMap.put("out", output[7]);
        		firewallMap.put("source", output[8]);
        		firewallMap.put("destination", output[9]);
        		firewallMap.put("protocol", output[4]);
        		firewallMap.put("target", output[3]);
        		firewallMap.put("options", temp);
        		dataNat.add(firewallMap);
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		if (reload) {
        			model.setRowCount(0);
        			reload=false;
        		}
        		model.addRow(new Object[]{output[0], "POSTROUTING", output[8], output[9], output[4], output[6], output[7], output[3], temp});
        		temp="";
        	}
        	p.waitFor();
        	p.destroy();
        	file.delete();
        	if (count==0) { //Nel caso non avessimo elementi in tabella...
        		DefaultTableModel model = (DefaultTableModel) table.getModel();
        		model.setRowCount(0);
        	} else {
        		count=0;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIptablesGui = new JFrame();
		frmIptablesGui.setIconImage(Toolkit.getDefaultToolkit().getImage(IPTGuiMain.class.getResource("/gui/iptgui.png")));
		frmIptablesGui.setTitle("IPTables GUI");
		frmIptablesGui.setBounds(100, 100, 900, 600);
		frmIptablesGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmIptablesGui.getContentPane().setLayout(springLayout);
		final JComboBox comboBox = new JComboBox();
		final JMenuItem defaultPolicy = new JMenuItem("Set Default Policy");
		
		/**************************************************
		 * REMOVE BUTTON
		 * Remove a rule from the corresponding table.
		 **************************************************/
		
		final JLabel label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				RemoveRule(comboBox.getSelectedItem().toString());
			}
		});
		label.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/remove.png")));
		frmIptablesGui.getContentPane().add(label);
		
		/**************************************************
		 * EDIT BUTTON
		 * Change a rule opening the IPTFrame interface.
		 **************************************************/
		
		JLabel label_1 = new JLabel("");
		springLayout.putConstraint(SpringLayout.WEST, label, 6, SpringLayout.EAST, label_1);
		springLayout.putConstraint(SpringLayout.SOUTH, label, 0, SpringLayout.SOUTH, label_1);
		springLayout.putConstraint(SpringLayout.EAST, label_1, -48, SpringLayout.EAST, frmIptablesGui.getContentPane());
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index=table.getSelectedRow();
				if (index==-1) {
					JOptionPane.showMessageDialog(null, "First you have to select a rule...", "Attention", JOptionPane.INFORMATION_MESSAGE);
				} else {
					IPTFrame addFrame ;
					if (comboBox.getSelectedItem().toString().equals("FILTER"))
						addFrame = new IPTFrame("edit", comboBox.getSelectedItem().toString(), data.get(index));
					else addFrame = new IPTFrame("edit", comboBox.getSelectedItem().toString(), dataNat.get(index));
					addFrame.setVisible(true);
				}
			}
		});
		label_1.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/edit.png")));
		frmIptablesGui.getContentPane().add(label_1);
		
		/**************************************************
		 * ADD BUTTON
		 * Add a rule in a specific chain.
		 **************************************************/
		
		JLabel label_2 = new JLabel("");
		springLayout.putConstraint(SpringLayout.EAST, label_2, -12, SpringLayout.WEST, label_1);
		springLayout.putConstraint(SpringLayout.SOUTH, label_1, 0, SpringLayout.SOUTH, label_2);
		label_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				IPTFrame addFrame = new IPTFrame("add", comboBox.getSelectedItem().toString(), null);
				addFrame.setVisible(true);
			}
		});
		label_2.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/add.png")));
		frmIptablesGui.getContentPane().add(label_2);
		
		JScrollPane scrollPane = new JScrollPane();
		springLayout.putConstraint(SpringLayout.WEST, scrollPane, 12, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, scrollPane, -12, SpringLayout.EAST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, label_2, -6, SpringLayout.NORTH, scrollPane);
		springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 51, SpringLayout.NORTH, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, 265, SpringLayout.NORTH, frmIptablesGui.getContentPane());
		frmIptablesGui.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setToolTipText("");
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Index", "Chain", "Source", "Destination", "Protocol", "In", "Out", "Target", "Others"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class, String.class, Object.class, Object.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setMaxWidth(50);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(120);
		table.getColumnModel().getColumn(1).setMinWidth(120);
		table.getColumnModel().getColumn(1).setMaxWidth(120);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setMinWidth(100);
		table.getColumnModel().getColumn(2).setMaxWidth(100);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(3).setMinWidth(100);
		table.getColumnModel().getColumn(3).setMaxWidth(100);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(4).setPreferredWidth(80);
		table.getColumnModel().getColumn(4).setMinWidth(80);
		table.getColumnModel().getColumn(4).setMaxWidth(80);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(5).setPreferredWidth(60);
		table.getColumnModel().getColumn(5).setMinWidth(60);
		table.getColumnModel().getColumn(5).setMaxWidth(60);
		table.getColumnModel().getColumn(6).setResizable(false);
		table.getColumnModel().getColumn(6).setPreferredWidth(60);
		table.getColumnModel().getColumn(6).setMinWidth(60);
		table.getColumnModel().getColumn(6).setMaxWidth(60);
		table.getColumnModel().getColumn(7).setResizable(false);
		table.getColumnModel().getColumn(7).setPreferredWidth(70);
		table.getColumnModel().getColumn(7).setMinWidth(70);
		table.getColumnModel().getColumn(7).setMaxWidth(70);
		table.getColumnModel().getColumn(8).setPreferredWidth(70);
		table.getColumnModel().getColumn(8).setMinWidth(70);
		table.setBounds(12, 55, 672, 140);
		scrollPane.setViewportView(table);
		
		/****************************
		 * SHELL EDITOR
		 * It allows the user to issue commands
		 ****************************/
			
		comboBox.addItem("FILTER");
		comboBox.addItem("NAT");
		final JButton btnRunCommand = new JButton("Run Command");
		final JEditorPane dtrpnIptables = new JEditorPane();
		dtrpnIptables.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					btnRunCommand.doClick();
				}
			}
		});
		dtrpnIptables.setCaretColor(new Color(255,255,255));
		btnRunCommand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				executeCommand("./runCmd.sh", dtrpnIptables.getText());
				if (comboBox.getSelectedItem().toString().equals("FILTER"))
					loadFilter();
				else loadNat();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, dtrpnIptables, 77, SpringLayout.SOUTH, scrollPane);
		springLayout.putConstraint(SpringLayout.WEST, dtrpnIptables, 12, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, dtrpnIptables, -101, SpringLayout.SOUTH, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, dtrpnIptables, 884, SpringLayout.WEST, frmIptablesGui.getContentPane());
		dtrpnIptables.setForeground(UIManager.getColor("Button.highlight"));
		dtrpnIptables.setBackground(UIManager.getColor("CheckBoxMenuItem.foreground"));
		dtrpnIptables.setText("iptables -t "+comboBox.getSelectedItem().toString().toLowerCase()+" ");
		frmIptablesGui.getContentPane().add(dtrpnIptables);
		
		JLabel lblIptablesCommandLine = new JLabel("IPTables Command Line");
		springLayout.putConstraint(SpringLayout.WEST, lblIptablesCommandLine, 10, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblIptablesCommandLine, -6, SpringLayout.NORTH, dtrpnIptables);
		frmIptablesGui.getContentPane().add(lblIptablesCommandLine);
		
		springLayout.putConstraint(SpringLayout.WEST, btnRunCommand, 750, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnRunCommand, 884, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnRunCommand, 6, SpringLayout.SOUTH, dtrpnIptables);
		frmIptablesGui.getContentPane().add(btnRunCommand);
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!startup) {
					if (comboBox.getSelectedItem().equals("FILTER"))
						loadFilter();
					else loadNat();
				}
				if (comboBox.getSelectedItem().toString().equals("NAT"))
					defaultPolicy.setVisible(false);
				else defaultPolicy.setVisible(true);
				dtrpnIptables.setText("iptables -t "+comboBox.getSelectedItem().toString().toLowerCase()+" ");
			}
		});
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 12, SpringLayout.WEST, frmIptablesGui.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, comboBox, -6, SpringLayout.NORTH, scrollPane);
		springLayout.putConstraint(SpringLayout.EAST, comboBox, 162, SpringLayout.WEST, frmIptablesGui.getContentPane());
		frmIptablesGui.getContentPane().add(comboBox);
		
		/****************************
		 * MENU BAR
		 * Set the Menu items.
		 ****************************/
		
		// File
		
		JMenuBar menuBar = new JMenuBar();
		frmIptablesGui.setJMenuBar(menuBar);
		JMenu file = new JMenu("File");
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		exit.setHorizontalAlignment(SwingConstants.LEFT);
		JMenuItem add = new JMenuItem("Add Rule");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IPTFrame addFrame = new IPTFrame("add", comboBox.getSelectedItem().toString(), null);
				addFrame.setVisible(true);
			}
		});
		add.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/add_small.png")));
		JMenuItem edit = new JMenuItem("Edit Rule");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=table.getSelectedRow();
				if (index==-1) {
					JOptionPane.showMessageDialog(null, "First you have to select a rule...", "Attention", JOptionPane.INFORMATION_MESSAGE);
				} else {
					IPTFrame addFrame;
					if (comboBox.getSelectedItem().toString().equals("FILTER"))
						addFrame = new IPTFrame("edit", comboBox.getSelectedItem().toString(), data.get(index));
					else addFrame = new IPTFrame("edit", comboBox.getSelectedItem().toString(), dataNat.get(index));
					addFrame.setVisible(true);
				}
			}
		});
		edit.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/edit_small.png")));
		JMenuItem remove = new JMenuItem("Remove Rule");
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RemoveRule(comboBox.getSelectedItem().toString());
			}
		});
		remove.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/remove_small.png")));
		file.add(add);
		file.add(edit);
		file.add(remove);
		file.add(exit);
		
		// Table
		
		JMenu table = new JMenu("Table");
		
		// Save
		
		JMenuItem save = new JMenuItem("Save Table");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser save = new JFileChooser();
				save.setDialogTitle("Save Table");
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Firewall file (.fw)", "fw", "text");
				save.setFileFilter(filter);
				save.showSaveDialog(frmIptablesGui);
				File fwFile = save.getSelectedFile();
				String s;
				
				if (fwFile!=null) 
					executeCommand("./save.sh", "iptables-save > "+fwFile.getAbsolutePath()+".fw");
			}
		});
		save.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/save_small.png")));
		
		// Load
		
		JMenuItem load = new JMenuItem("Load Table");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser load = new JFileChooser();
				load.setDialogTitle("Load Table");
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Firewall file (.fw)", "fw", "text");
				load.setFileFilter(filter);
				load.showOpenDialog(frmIptablesGui);
				File fwFile = load.getSelectedFile();
				if (fwFile!=null) {
					executeCommand("./load.sh", "iptables-restore < "+fwFile.getAbsolutePath());
					if (comboBox.getSelectedItem().toString().equals("FILTER"))
						loadFilter();
					else loadNat();
					
				}
			}
		});
		load.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/load_small.png")));

		
		// Set Default Policy
		
		defaultPolicy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IPTDefault dialog = new IPTDefault();
				dialog.setVisible(true);
			}
		});
		defaultPolicy.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/settings.png")));
		table.add(save);
		table.add(load);
		table.add(defaultPolicy);
		
		// Help
		
		JMenu help = new JMenu("Help");
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AboutFrame frame = new AboutFrame();
				frame.setVisible(true);
			}
		});
		about.setIcon(new ImageIcon(IPTGuiMain.class.getResource("/gui/about_small.png")));
		help.add(about);
		
		// Set MenuBar
		
		menuBar.add(file);
		menuBar.add(table);
		menuBar.add(help);
	
    }
	
	static void executeCommand(String fileName, String command) {
		String s="";
		String sErr="";
		try {
			File file = new File(fileName);
			String[] cmd = {"#! /bin/sh", command};
			if (_DEBUG_)
				System.out.println(cmd[1]);
			else log.write(cmd[1], "CMD");
			PrintWriter writer = new PrintWriter(fileName, "UTF-8");
			writer.println(cmd[0]+"\n"+cmd[1]);
			writer.close();
			file.setWritable(true);
			file.setExecutable(true);

			Process p = Runtime.getRuntime().exec(sudo+fileName);
			BufferedReader br = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
			BufferedReader brError = new BufferedReader(
					new InputStreamReader(p.getErrorStream()));
			String cmdOut="";
			while (((s = br.readLine()) != null) || ((sErr = brError.readLine()) != null)) {
				if (sErr != null)
					cmdOut += sErr;
				if (s != null)
					cmdOut += s;
				cmdOut += "\n";
			}
			if (!cmdOut.equals("")) {
				if (_DEBUG_)
					System.out.println(cmdOut);
				else log.write(cmdOut, "INFO");
				JOptionPane.showMessageDialog(null, cmdOut, "Output", JOptionPane.INFORMATION_MESSAGE);
			}
			p.waitFor();
			p.destroy();
			file.delete();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, "Something went wrong during the execution of command...", "Error", JOptionPane.ERROR_MESSAGE);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, "Something went wrong during the execution of command...", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private static void RemoveRule(String tab) {
		int index=table.getSelectedRow();
		if (index==-1) {
			JOptionPane.showMessageDialog(null, "First you have to select a rule...", "Attention", JOptionPane.INFORMATION_MESSAGE);
		} else {
			if (tab.equals("FILTER")) {
				executeCommand("./filter_remove.sh", "iptables -t "+tab.toLowerCase()+" -D "+data.get(index).get("chain").toString()+" "+data.get(index).get("index").toString());
				loadFilter();
			}
			else {
				executeCommand("./filter_remove.sh", "iptables -t "+tab.toLowerCase()+" -D "+dataNat.get(index).get("chain").toString()+" "+dataNat.get(index).get("index").toString());
				loadNat();
			}
		}
	}
}
