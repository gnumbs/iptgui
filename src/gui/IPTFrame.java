package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JCheckBox;
import java.awt.Toolkit;

public class IPTFrame extends JFrame {
	private JTextField textField;
	private JTextField textField_4;
	private JTextField textField_1;
	private JTextField textField_1_1;
	private JTextField textField_2;
	private JTextField textField_2_1;
	private JTextField textField_3;
	private JTextField textField_3_1;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Create the frame.
	 */
	public IPTFrame(String type, final String table, Map<String, String> data) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(IPTFrame.class.getResource("/gui/iptgui.png")));
		if (type.equals("edit"))
			setTitle("Edit Rule");
		else	
			setTitle("Add Rule");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 781, 557);
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);
		
		/*************************
		 * TEXT INITIALIZATION
		*************************/
		
		textField = new JTextField();
		textField_1 = new JTextField();
		textField_2 = new JTextField();
		textField_3 = new JTextField();
		textField_4 = new JTextField();
		textField_6 = new JTextField();
		textField_7 = new JTextField();
		textField_8 = new JTextField();
		JLabel lblTo = new JLabel("To");
		if (table.equals("FILTER")) {
			lblTo.setVisible(false);
			textField_8.setVisible(false);
		}
		springLayout.putConstraint(SpringLayout.WEST, textField_7, 11, SpringLayout.EAST, textField_6);
		springLayout.putConstraint(SpringLayout.EAST, textField_7, 70, SpringLayout.EAST, textField_6);
		springLayout.putConstraint(SpringLayout.EAST, textField_4, -10, SpringLayout.EAST, getContentPane());
		textField_5 = new JTextField();
		textField_2_1 = new JTextField();
		textField_2_1.setEnabled(false);
		textField_1_1 = new JTextField();
		textField_3_1 = new JTextField();
		textField_3_1.setEnabled(false);
		
		/*************************
		 * STATE CHECKBOX
		*************************/
		
		final JCheckBox chckbxEstablished = new JCheckBox("ESTABLISHED");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxEstablished, 161, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, chckbxEstablished, 346, SpringLayout.WEST, getContentPane());
		final JCheckBox chckbxRelated = new JCheckBox("RELATED");
		springLayout.putConstraint(SpringLayout.EAST, chckbxEstablished, -6, SpringLayout.WEST, chckbxRelated);
		springLayout.putConstraint(SpringLayout.WEST, chckbxRelated, 469, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, chckbxRelated, 554, SpringLayout.WEST, getContentPane());
		final JCheckBox chckbxInvalid = new JCheckBox("INVALID");
		springLayout.putConstraint(SpringLayout.WEST, chckbxInvalid, 6, SpringLayout.EAST, chckbxRelated);
		springLayout.putConstraint(SpringLayout.EAST, chckbxInvalid, 85, SpringLayout.EAST, chckbxRelated);
		final JCheckBox chckbxNewCheckBox = new JCheckBox("NEW");
		springLayout.putConstraint(SpringLayout.EAST, chckbxNewCheckBox, -9, SpringLayout.WEST, chckbxEstablished);
		
		/***************************************************************************
		 * CHECKBOX MANAGEMENT
		 * "chckbxInvalid" is mutually exclusive with other checkboxes.
		 ***************************************************************************/
		
		if (table.equals("NAT")) {
			chckbxNewCheckBox.setEnabled(false);
			chckbxEstablished.setEnabled(false);
			chckbxRelated.setEnabled(false);
			chckbxInvalid.setEnabled(false);
		} else {
			chckbxNewCheckBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()==ItemEvent.SELECTED) {
						chckbxInvalid.setEnabled(false);
					} else {
						if (!chckbxEstablished.isSelected() && !chckbxRelated.isSelected())
							chckbxInvalid.setEnabled(true);
					}
				}
			});

			chckbxInvalid.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()==ItemEvent.SELECTED) {
						chckbxNewCheckBox.setEnabled(false);
						chckbxEstablished.setEnabled(false);
						chckbxRelated.setEnabled(false);
					} else {
						chckbxNewCheckBox.setEnabled(true);
						chckbxEstablished.setEnabled(true);
						chckbxRelated.setEnabled(true);
					}
				}
			});

			chckbxEstablished.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()==ItemEvent.SELECTED) {
						chckbxInvalid.setEnabled(false);
					} else {
						if (!chckbxNewCheckBox.isSelected() && !chckbxRelated.isSelected())
							chckbxInvalid.setEnabled(true);
					}
				}
			});

			chckbxRelated.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()==ItemEvent.SELECTED) {
						chckbxInvalid.setEnabled(false);
					} else {
						if (!chckbxNewCheckBox.isSelected() && !chckbxEstablished.isSelected())
							chckbxInvalid.setEnabled(true);
					}
				}
			});
		}
		
		
		/*************************
		 * PROTOCOL - ! -
		 * Not TCP or Not UDP
		*************************/
		
		final JCheckBox checkBox = new JCheckBox("!");
		checkBox.setEnabled(false);
		checkBox.setVisible(false);
		
		/*************************
		 * COMBOBOX POSITION
		*************************/
		
		final JComboBox comboBox_3 = new JComboBox();
		springLayout.putConstraint(SpringLayout.WEST, comboBox_3, 366, SpringLayout.WEST, checkBox);
		springLayout.putConstraint(SpringLayout.EAST, comboBox_3, 0, SpringLayout.EAST, textField_4);
		final JComboBox comboBox = new JComboBox();
		springLayout.putConstraint(SpringLayout.EAST, comboBox, -9, SpringLayout.EAST, getContentPane());
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getItem().equals("INPUT") || e.getItem().equals("PREROUTING")) {
					textField_6.setEnabled(true);
					textField_7.setEnabled(false);
				} else if (e.getItem().equals("OUTPUT") || e.getItem().equals("POSTROUTING")) {
					textField_6.setEnabled(false);
					textField_7.setEnabled(true);
				} else { //FORWARD
					textField_6.setEnabled(true);
					textField_7.setEnabled(true);
				}
					
			}
		});
				
		final JComboBox comboBox_1 = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBox_1, 160, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, comboBox_1, 8, SpringLayout.EAST, checkBox);
		springLayout.putConstraint(SpringLayout.EAST, comboBox_1, 78, SpringLayout.EAST, checkBox);
		comboBox_1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
		        if (e.getItem().equals("TCP") || e.getItem().equals("UDP")) {
		        	checkBox.setEnabled(true);
		        	textField_2_1.setEnabled(true);
		        	textField_3_1.setEnabled(true);
		        }
		        else {
		        	checkBox.setEnabled(false);
		        	textField_2_1.setEnabled(false);
		        	textField_3_1.setEnabled(false);
		        }
			}
		});
		final JComboBox comboBox_4 = new JComboBox();
		comboBox_4.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getItem().equals("INSERT") || e.getItem().equals("REPLACE")) {
					textField_5.setEnabled(true);
				} else {
					textField_5.setEnabled(false);
				}
					
			}
		});
		
		/*************************
		 * COMBOBOX ITEM		
		*************************/
		
		// Chain
		
		if (table.equals("FILTER")) {
			comboBox.addItem("INPUT");
			comboBox.addItem("OUTPUT");
			comboBox.addItem("FORWARD");
		} else {
			comboBox.addItem("PREROUTING");
			comboBox.addItem("INPUT");
			comboBox.addItem("OUTPUT");
			comboBox.addItem("POSTROUTING");
		}
		
		// Protocol
		
		comboBox_1.addItem("ALL");
		comboBox_1.addItem("TCP");
		comboBox_1.addItem("UDP");
		
		// Target
		if (table.equals("FILTER")) {
			comboBox_3.addItem("ACCEPT");
			comboBox_3.addItem("DROP");
			comboBox_3.addItem("RETURN");
		} else {
			comboBox_3.addItem("DNAT");
			comboBox_3.addItem("SNAT");
		}
		
		// Action
		
		comboBox_4.addItem("APPEND");
		comboBox_4.addItem("INSERT");
		comboBox_4.addItem("CHECK");
		if (type.equals("edit")) { 
			comboBox_4.addItem("REPLACE");
		}
		
		if (type.equals("edit")) {
			comboBox.setSelectedItem(data.get("chain"));
			textField_4.setText(data.get("source"));
			textField_1_1.setText(data.get("destination"));
			if (data.get("in").equals("*"))
				textField_6.setText("");
			else
				textField_6.setText(data.get("in"));
			if (data.get("out").equals("*"))
				textField_7.setText("");
			else
				textField_7.setText(data.get("out"));
			String opt = data.get("options");
			if (opt.contains("dpt:")) {
				String sub = opt.substring(opt.indexOf("dpt:"), opt.length()-1);
				String[] parts = sub.split(":",2)[1].split(" ");
				textField_3_1.setText(parts[0]);
			}
			if (opt.contains("spt:")) {
				String sub = opt.substring(opt.indexOf("spt:"), opt.length()-1);
				String[] parts = sub.split(":",2)[1].split(" ");
				textField_2_1.setText(parts[0]);
			}
			if (opt.contains("ctstate")) {
				String sub = opt.substring(opt.indexOf("ctstate"), opt.length()-1);
				String[] parts = sub.split(" ")[1].split(",");
				for (int i=0; i<parts.length; i++) {
					if (parts[i].equals("NEW"))
						chckbxNewCheckBox.setSelected(true);
					if (parts[i].equals("ESTABLISHED"))
						chckbxEstablished.setSelected(true);
					if (parts[i].equals("RELATED"))
						chckbxRelated.setSelected(true);
					if (parts[i].equals("INVALID"))
						chckbxInvalid.setSelected(true);
				}
			}
			if (opt.contains("to:")) {
				String sub = opt.substring(opt.indexOf("to:"), opt.length()-1);
				String[] parts = sub.split(":",2)[1].split(" ");
				textField_8.setText(parts[0]);
			}
			comboBox_1.setSelectedItem(data.get("protocol").toUpperCase());
			comboBox_3.setSelectedItem(data.get("target"));
			textField_5.setText(data.get("index"));
			comboBox_4.setSelectedItem("REPLACE");
			comboBox_4.setEnabled(false);;
		}
		
		/*************************
		 * LABEL AND TEXT
		*************************/
		
		JLabel lblSelezionaCatena = new JLabel("Seleziona Catena");
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 141, SpringLayout.EAST, lblSelezionaCatena);
		springLayout.putConstraint(SpringLayout.NORTH, lblSelezionaCatena, 10, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, comboBox, -5, SpringLayout.NORTH, lblSelezionaCatena);
		springLayout.putConstraint(SpringLayout.WEST, lblSelezionaCatena, 10, SpringLayout.WEST, getContentPane());
		getContentPane().add(lblSelezionaCatena);
		
		JLabel lblSourceAddress = new JLabel("Source Address:");
		springLayout.putConstraint(SpringLayout.NORTH, textField_4, -2, SpringLayout.NORTH, lblSourceAddress);
		springLayout.putConstraint(SpringLayout.WEST, textField_4, 149, SpringLayout.EAST, lblSourceAddress);
		springLayout.putConstraint(SpringLayout.NORTH, lblSourceAddress, 22, SpringLayout.SOUTH, lblSelezionaCatena);
		springLayout.putConstraint(SpringLayout.WEST, lblSourceAddress, 0, SpringLayout.WEST, lblSelezionaCatena);
		lblSourceAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblSourceAddress);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		springLayout.putConstraint(SpringLayout.NORTH, chckbxRelated, 64, SpringLayout.SOUTH, textField_1_1);
		springLayout.putConstraint(SpringLayout.SOUTH, chckbxRelated, 87, SpringLayout.SOUTH, textField_1_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_1_1, 0, SpringLayout.WEST, textField_4);
		springLayout.putConstraint(SpringLayout.EAST, textField_1_1, -10, SpringLayout.EAST, getContentPane());
		textField_1_1.setColumns(10);
		getContentPane().add(textField_1_1);
		
		JLabel lblDestinationAddress = new JLabel("Destination Address:");
		springLayout.putConstraint(SpringLayout.NORTH, textField_1_1, -2, SpringLayout.NORTH, lblDestinationAddress);
		springLayout.putConstraint(SpringLayout.NORTH, lblDestinationAddress, 18, SpringLayout.SOUTH, lblSourceAddress);
		springLayout.putConstraint(SpringLayout.WEST, lblDestinationAddress, 0, SpringLayout.WEST, lblSelezionaCatena);
		lblDestinationAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblDestinationAddress);
		
		JLabel lblSourcePort = new JLabel("Source Port");
		
		springLayout.putConstraint(SpringLayout.NORTH, textField_7, -2, SpringLayout.NORTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.EAST, textField_2_1, 90, SpringLayout.EAST, lblSourcePort);
		springLayout.putConstraint(SpringLayout.NORTH, textField_3_1, -2, SpringLayout.NORTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.WEST, textField_2_1, 20, SpringLayout.EAST, lblSourcePort);
		springLayout.putConstraint(SpringLayout.NORTH, checkBox, 23, SpringLayout.SOUTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.NORTH, lblSourcePort, 123, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, textField_2_1, -2, SpringLayout.NORTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.WEST, lblSourcePort, 0, SpringLayout.WEST, lblSelezionaCatena);
		getContentPane().add(lblSourcePort);
		getContentPane().add(textField_2_1);
		textField_2_1.setColumns(10);
		
		JLabel lblDestinationPort = new JLabel("Destination Port");
		springLayout.putConstraint(SpringLayout.WEST, lblDestinationPort, 6, SpringLayout.EAST, textField_2_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_3_1, 6, SpringLayout.EAST, lblDestinationPort);
		springLayout.putConstraint(SpringLayout.NORTH, chckbxNewCheckBox, 23, SpringLayout.SOUTH, lblDestinationPort);
		springLayout.putConstraint(SpringLayout.NORTH, lblDestinationPort, 0, SpringLayout.NORTH, lblSourcePort);
		getContentPane().add(lblDestinationPort);
		
		JLabel lblProtocol = new JLabel("Protocol");
		springLayout.putConstraint(SpringLayout.NORTH, chckbxInvalid, -4, SpringLayout.NORTH, lblProtocol);
		springLayout.putConstraint(SpringLayout.WEST, checkBox, 6, SpringLayout.EAST, lblProtocol);
		springLayout.putConstraint(SpringLayout.NORTH, lblProtocol, 27, SpringLayout.SOUTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.SOUTH, lblProtocol, 42, SpringLayout.SOUTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.WEST, lblProtocol, 10, SpringLayout.WEST, getContentPane());
		getContentPane().add(lblProtocol);
		getContentPane().add(comboBox);
		getContentPane().add(comboBox_1);
		
		JLabel lblState = new JLabel("State");
		springLayout.putConstraint(SpringLayout.WEST, chckbxNewCheckBox, 21, SpringLayout.EAST, lblState);
		springLayout.putConstraint(SpringLayout.WEST, lblState, 37, SpringLayout.EAST, comboBox_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblState, 27, SpringLayout.SOUTH, lblDestinationPort);
		springLayout.putConstraint(SpringLayout.EAST, lblState, 76, SpringLayout.EAST, comboBox_1);
		getContentPane().add(lblState);
		
		/**************************************************
		 * OK BUTTON
		 * Format and executes iptables command.. 		
		 **************************************************/
		
		JButton btnOk = new JButton("OK");
		springLayout.putConstraint(SpringLayout.SOUTH, comboBox_3, -54, SpringLayout.NORTH, btnOk);
		springLayout.putConstraint(SpringLayout.EAST, btnOk, -10, SpringLayout.EAST, getContentPane());
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
					String cmd = "iptables -t "+table.toLowerCase()+" ";

					// Select Action

					if (comboBox_4.getSelectedItem().toString().equals("APPEND")) {
						cmd += "-A ";
					} else if (comboBox_4.getSelectedItem().toString().equals("INSERT")) {
						cmd += "-I ";
					} else if (comboBox_4.getSelectedItem().toString().equals("REPLACE")) {
						cmd += "-R ";
					} else cmd += "-C ";

					// Select Chain

					cmd += comboBox.getSelectedItem().toString()+" ";
					
					if ((comboBox_4.getSelectedItem().toString().equals("INSERT") || comboBox_4.getSelectedItem().toString().equals("REPLACE")) && !textField_5.getText().isEmpty() && isNumeric(textField_5.getText())) {
						cmd += textField_5.getText()+" ";
					}
						
					// Select interfaces
					
					if (!textField_6.getText().equals("") && textField_6.isEnabled())
						cmd += "-i "+textField_6.getText()+" ";
						
					if (!textField_7.getText().equals("") && textField_7.isEnabled())	
						cmd += "-o "+textField_7.getText()+" ";
					
					// Select Address

					if (!textField_4.getText().isEmpty()) 
						cmd += "-s "+textField_4.getText()+" ";
					if (!textField_1_1.getText().isEmpty())
						cmd += "-d "+textField_1_1.getText()+" ";

					// Select Protocol

					if (checkBox.isSelected())
						cmd += "! -p "+comboBox_1.getSelectedItem().toString().toLowerCase()+" ";
					else
						cmd += "-p "+comboBox_1.getSelectedItem().toString().toLowerCase()+" ";
						
					// Select SPort and DPort

					if (textField_2_1.isEnabled() && isNumeric (textField_2_1.getText()) && Integer.parseInt(textField_2_1.getText())>=0 && Integer.parseInt(textField_2_1.getText())<=65535) {
						cmd += "--sport "+textField_2_1.getText()+" ";
					}
					if (textField_3_1.isEnabled() && isNumeric (textField_3_1.getText()) && Integer.parseInt(textField_3_1.getText())>=0 && Integer.parseInt(textField_3_1.getText())<=65535) {
						cmd += "--dport "+textField_3_1.getText()+" ";
					}
					
					// Select state
					
					if (chckbxNewCheckBox.isSelected() || chckbxEstablished.isSelected() || chckbxRelated.isSelected() || chckbxInvalid.isSelected()) {
						cmd += "-m conntrack --ctstate ";
						if (chckbxNewCheckBox.isSelected())
							cmd += "NEW,";
						if (chckbxEstablished.isSelected())
							cmd += "ESTABLISHED,";
						if (chckbxRelated.isSelected())
							cmd += "RELATED";
						if (chckbxInvalid.isSelected())
							cmd += "INVALID";
						if (cmd.charAt(cmd.length()-1)==',') {
							cmd = cmd.substring(0,cmd.length()-1);
						}
					}
					
					// Select Target
					
					cmd += " -j "+comboBox_3.getSelectedItem().toString();
					
					// Select To if using NAT table
					
					if (table.equals("NAT") && !textField_8.getText().equals(""))
						cmd += " --to "+textField_8.getText();
					
					// Print complete command
					
					if (IPTGuiMain._DEBUG_)
						System.out.println(cmd);
					else IPTGuiMain.log.write(cmd, "CMD");

					// Executes the command
					
					Process p;
					String s="";
					String sErr="";
					try {
			        	File file = new File("./insert.sh");
			        	String ruleCmd = "#! /bin/sh \n"+cmd;
			        	
			        	PrintWriter writer = new PrintWriter("./insert.sh", "UTF-8");
			        	writer.println(ruleCmd);
			        	writer.close();
			        	file.setWritable(true);
			        	file.setExecutable(true);
						p = Runtime.getRuntime().exec(IPTGuiMain.sudo+"./insert.sh");
		    			BufferedReader br = new BufferedReader(
		    					new InputStreamReader(p.getInputStream()));
		    			BufferedReader brError = new BufferedReader(
		    					new InputStreamReader(p.getErrorStream()));
		    			String cmdOut="";
		    			while (((s = br.readLine()) != null) || ((sErr = brError.readLine()) != null)) {
		    				if (sErr != null)
		    					cmdOut += sErr;
		    				if (s != null)
		    					cmdOut += s;
		    				cmdOut += "\n";
		    			}
		    			if (!cmdOut.equals("")) {
		    				if (IPTGuiMain._DEBUG_)
		    					System.out.println(cmdOut);
		    				else IPTGuiMain.log.write(cmdOut, "ERROR");
			    			JOptionPane.showMessageDialog(null, cmdOut, "Error", JOptionPane.ERROR_MESSAGE);
		    			}
		    			p.waitFor();
		    			p.destroy();
		    			file.delete();
		    			if (table.equals("FILTER"))
		    				IPTGuiMain.loadFilter();
		    			else
		    				IPTGuiMain.loadNat();
		    			if (comboBox_4.getSelectedItem().equals("REPLACE")) {
		    				setVisible(false);
		    				dispose();
		    			}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, btnOk, -10, SpringLayout.SOUTH, getContentPane());
		getContentPane().add(btnOk);
		
		/*************************
		 * CANCEL BUTTON	
		 * Dispose JFrame.	
		 *************************/
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				dispose();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnCancel, 0, SpringLayout.NORTH, btnOk);
		springLayout.putConstraint(SpringLayout.EAST, btnCancel, -12, SpringLayout.WEST, btnOk);
		getContentPane().add(btnCancel);
		textField_3_1.setColumns(10);
		getContentPane().add(textField_3_1);
		
		JLabel lblTarget = new JLabel("Target");
		springLayout.putConstraint(SpringLayout.NORTH, lblTarget, 5, SpringLayout.NORTH, comboBox_3);
		springLayout.putConstraint(SpringLayout.EAST, lblTarget, -13, SpringLayout.WEST, comboBox_3);
		getContentPane().add(lblTarget);
		getContentPane().add(comboBox_3);
		
		JLabel lblAction = new JLabel("Action");
		springLayout.putConstraint(SpringLayout.NORTH, textField_5, -2, SpringLayout.NORTH, lblAction);
		springLayout.putConstraint(SpringLayout.EAST, comboBox_4, 184, SpringLayout.EAST, lblAction);
		springLayout.putConstraint(SpringLayout.WEST, comboBox_4, 21, SpringLayout.EAST, lblAction);
		springLayout.putConstraint(SpringLayout.NORTH, lblAction, 38, SpringLayout.SOUTH, lblProtocol);
		springLayout.putConstraint(SpringLayout.NORTH, comboBox_4, -5, SpringLayout.NORTH, lblAction);
		springLayout.putConstraint(SpringLayout.WEST, lblAction, 0, SpringLayout.WEST, lblSelezionaCatena);
		getContentPane().add(lblAction);
		getContentPane().add(comboBox_4);
		getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNum = new JLabel("Num");
		springLayout.putConstraint(SpringLayout.WEST, textField_5, 6, SpringLayout.EAST, lblNum);
		springLayout.putConstraint(SpringLayout.EAST, textField_5, 65, SpringLayout.EAST, lblNum);
		springLayout.putConstraint(SpringLayout.NORTH, lblNum, 0, SpringLayout.NORTH, lblAction);
		springLayout.putConstraint(SpringLayout.WEST, lblNum, 6, SpringLayout.EAST, comboBox_4);
		getContentPane().add(lblNum);
		getContentPane().add(checkBox);
		getContentPane().add(chckbxNewCheckBox);
		getContentPane().add(chckbxEstablished);
		getContentPane().add(chckbxRelated);
		getContentPane().add(chckbxInvalid);
		
		JLabel lblInputInterface = new JLabel("Interface (IN - OUT)");
		springLayout.putConstraint(SpringLayout.EAST, textField_6, 65, SpringLayout.EAST, lblInputInterface);
		springLayout.putConstraint(SpringLayout.EAST, textField_3_1, -37, SpringLayout.WEST, lblInputInterface);
		springLayout.putConstraint(SpringLayout.WEST, lblInputInterface, 419, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblInputInterface, 0, SpringLayout.NORTH, lblSourcePort);
		getContentPane().add(lblInputInterface);
		
		springLayout.putConstraint(SpringLayout.NORTH, textField_6, -2, SpringLayout.NORTH, lblSourcePort);
		springLayout.putConstraint(SpringLayout.WEST, textField_6, 7, SpringLayout.EAST, lblInputInterface);
		getContentPane().add(textField_6);
		textField_6.setColumns(10);
		getContentPane().add(textField_7);
		textField_7.setColumns(10);
		
		springLayout.putConstraint(SpringLayout.NORTH, lblTo, 0, SpringLayout.NORTH, lblAction);
		springLayout.putConstraint(SpringLayout.WEST, lblTo, 6, SpringLayout.EAST, textField_5);
		getContentPane().add(lblTo);
		
		springLayout.putConstraint(SpringLayout.NORTH, textField_8, 32, SpringLayout.SOUTH, chckbxEstablished);
		springLayout.putConstraint(SpringLayout.WEST, textField_8, 6, SpringLayout.EAST, lblTo);
		springLayout.putConstraint(SpringLayout.EAST, textField_8, 404, SpringLayout.EAST, lblTo);
		getContentPane().add(textField_8);
		textField_8.setColumns(10);
		
	}
	
	/**************************************************
	 * ISNUMERIC
	 * Check if "str" is a numeric string.
	 **************************************************/
	
	public static boolean isNumeric(String str)  {  
	  try  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  { return false; }  
	  return true;  
	}
}
