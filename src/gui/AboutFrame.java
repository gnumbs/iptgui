package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class AboutFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AboutFrame frame = new AboutFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AboutFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(AboutFrame.class.getResource("/gui/iptgui.png")));
		setTitle("About IPTGui");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(AboutFrame.class.getResource("/gui/FedericoII.png")));
		sl_contentPane.putConstraint(SpringLayout.EAST, label, 0, SpringLayout.EAST, contentPane);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		sl_contentPane.putConstraint(SpringLayout.NORTH, label, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, label, 10, SpringLayout.WEST, contentPane);
		contentPane.add(label);
		
		JLabel lblIptguibeta = new JLabel("IPTGui (Beta)");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblIptguibeta, -92, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblIptguibeta, -163, SpringLayout.EAST, contentPane);
		lblIptguibeta.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblIptguibeta);
		
		JLabel lblNetworkSecurity = new JLabel("Network Security - 2016");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNetworkSecurity, 6, SpringLayout.SOUTH, lblIptguibeta);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblNetworkSecurity, -124, SpringLayout.EAST, contentPane);
		lblNetworkSecurity.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNetworkSecurity);
		
		JLabel lblUniversitDegliStudi = new JLabel("Università degli Studi di Napoli Federico II");
		lblUniversitDegliStudi.setHorizontalAlignment(SwingConstants.CENTER);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblUniversitDegliStudi, 7, SpringLayout.SOUTH, label);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblUniversitDegliStudi, -56, SpringLayout.EAST, contentPane);
		contentPane.add(lblUniversitDegliStudi);
		
		JLabel lblUmbertoFerrandino = new JLabel("Umberto Ferrandino - M63/503");
		sl_contentPane.putConstraint(SpringLayout.WEST, lblUmbertoFerrandino, 0, SpringLayout.WEST, label);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblUmbertoFerrandino, -31, SpringLayout.SOUTH, contentPane);
		contentPane.add(lblUmbertoFerrandino);
		
		JLabel lblEmailUferrandinostudentiuninait = new JLabel("Email: u.ferrandino@studenti.unina.it");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblEmailUferrandinostudentiuninait, 6, SpringLayout.SOUTH, lblUmbertoFerrandino);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblEmailUferrandinostudentiuninait, 0, SpringLayout.WEST, label);
		contentPane.add(lblEmailUferrandinostudentiuninait);
	}

}
