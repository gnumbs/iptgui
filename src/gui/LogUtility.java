package gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;

public class LogUtility {
	
	private String path;
	private String name;
	private File log;
	
	public LogUtility(String path, String name) {
		this.path=path;
		this.name=name;
		if (path.charAt(path.length()-1)=='/')
			this.log = new File(path+name);
		else this.log = new File(path+"/"+name);
		File dir = new File(path);
		
		try {
			if (!(dir.exists())) {
				dir.mkdir();
				dir.setReadable(true,false);
				dir.setWritable(true,false);
			}
			if (!this.log.exists()) {
				this.log.createNewFile();
				this.log.setReadable(true,false);
				this.log.setWritable(true,false);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void write(String msg, String severity) {
		try {
			String message = "["+severity+"] "+new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date())+": "+msg+"\n";
			Files.write(this.log.toPath(), message.getBytes(), StandardOpenOption.APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getPath() {
		return this.path;
	}
	
	public String getName() {
		return this.name;
	}
	
	public File getFile() {
		return this.log;
	}

}
