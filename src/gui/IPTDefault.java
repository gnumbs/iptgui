package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Toolkit;

public class IPTDefault extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			IPTDefault dialog = new IPTDefault();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public IPTDefault() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(IPTDefault.class.getResource("/gui/iptgui.png")));
		setTitle("Default Policy");
		setBounds(100, 100, 501, 146);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		
		final JComboBox comboBox = new JComboBox();
		sl_contentPanel.putConstraint(SpringLayout.WEST, comboBox, 5, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, comboBox, 174, SpringLayout.WEST, contentPanel);
		contentPanel.add(comboBox);
		
		comboBox.addItem("INPUT");
		comboBox.addItem("OUTPUT");
		comboBox.addItem("FORWARD");
		
		JLabel lblChain = new JLabel("Chain");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, comboBox, 6, SpringLayout.SOUTH, lblChain);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, comboBox, 30, SpringLayout.SOUTH, lblChain);
		sl_contentPanel.putConstraint(SpringLayout.WEST, lblChain, 5, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, lblChain, -49, SpringLayout.SOUTH, contentPanel);
		contentPanel.add(lblChain);
		
		final JComboBox comboBox_1 = new JComboBox();
		sl_contentPanel.putConstraint(SpringLayout.WEST, comboBox_1, 34, SpringLayout.EAST, comboBox);
		sl_contentPanel.putConstraint(SpringLayout.EAST, comboBox_1, 203, SpringLayout.EAST, comboBox);
		contentPanel.add(comboBox_1);
		
		comboBox_1.addItem("ACCEPT");
		comboBox_1.addItem("DROP");
		
		JLabel lblTarget = new JLabel("Target");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, comboBox_1, 6, SpringLayout.SOUTH, lblTarget);
		sl_contentPanel.putConstraint(SpringLayout.WEST, lblTarget, 163, SpringLayout.EAST, lblChain);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, lblTarget, -49, SpringLayout.SOUTH, contentPanel);
		contentPanel.add(lblTarget);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						String cmd = "iptables -P "+comboBox.getSelectedItem().toString()+" "+comboBox_1.getSelectedItem().toString();
						IPTGuiMain.executeCommand("./filter_default.sh", cmd);
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
