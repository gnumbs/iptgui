============================================================

Title:		IPTGui, a simple interface for iptables

Author:		Umberto Ferrandino, M63/503

Email address:	u.ferrandino@studenti.unina.it

============================================================

DESCRIPTION
-----------

  IPTGui is a simple gui for itpables, written in Java with Swing framework and bash. 
  Be careful using IPTGui: iptables is a root tool, so the software will ask for your password via kdesudo/gksudo.
  Do not be fooled by the fact that this is a Java (and then cross-platform) software! 
  The program is based on bash iptables commands, working only under GNU/Linux operating system.
    
REQUIREMENTS
------------

  IPTGui requires the installation of:

  - kdesudo or gksudo: choose the one or the other based on your DE..But I truly suggest kdesudo.
  - Java JRE: the program is distributed in a .jar file, so you need java to use it.
  - iptables: obviously.
 
  If you want to recompile the project, you will also need Java JDK. 
 
  - Import the project (for example) in Eclipse and rebuild it.

INSTALLATION
------------

  Simply run the install.sh script in the IPTGui folder.
 
    - $ chmod +x install.sh
    - $ ./install.sh
 
  The script will check the required software and then install IPTGui in the destination folder. 
  Run the program with the following command:
 
    - $ java -jar your_IPTGui_installationPath/IPTGui.jar  
    
NOTES
-----

  Sometimes IPTGui can be invasive, often asking for root password. As mentioned, iptables is a root tool, so every 
  operation it performs is a root command. You can run it with root privileges (depending on Linux distribution):

    - $ kdesu 'java -jar your_IPTGui_installationPath/IPTGui.jar'
    - $ sudo java -jar your_IPTGui_installationPath/IPTGui.jar (if permitted)
    - $ gksu 'java -jar your_IPTGui_installationPath/IPTGui.jar'
    
  In *Buntu distribution you can simply use sudo.
  In other distribution sudo may not be able to run graphics applications, so you need to use kdesu/gksu(or kdesudo/gksudo).

  Otherwise you can append these lines to /etc/sudoers, to avoid the password being asked every time.
 
  First of all open /etc/sudoers (using visudo)
 
    - # visudo
    
  Append to the file the following line:
 
    - your_user	ALL=(ALL) NOPASSWD: your_IPTGui_installationPath/script_Name.sh
 
  Where "script_Name.sh" can be:
 
    - IPTGui.sh: used for loading INPUT chain rules
    - filterOutput_load.sh: used for loading OUTPUT chain rules
    - filterForward_load.sh: used for loading FORWARD chain rules
    - natPre_load.sh: user for loading nat PREROUTING chain rules
    - natIn_load.sh: user for loading nat INPUT chain rules
    - natOut_load.sh: user for loading nat OUTPUT chain rules
    - natPost_load.sh: user for loading nat POSTROUTING chain rules
    - insert.sh: used to insert a rule
    - filter_remove.sh: used for removing a rule
    - save.sh: used for saving table in a .fw file
    - load.sh: used for loading table from .fw file
    
  Pay attention! These script will be executed as root without asking for password! Can be a way for privilege escalation.

USEFUL LINKS
------------

  - Kdesudo Manual: http://manpages.ubuntu.com/manpages/precise/en/man1/kdesudo.1.html
  - Gksudo Manual: http://linux.die.net/man/1/gksudo
  - Sudo Manual: http://linux.die.net/man/8/sudo
  - Iptables Manual: http://ipset.netfilter.org/iptables.man.html

CREDITS
------- 

  As always I have to thank StackOverflow community.
    

 
 
